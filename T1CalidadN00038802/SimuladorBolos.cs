﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using T1CalidadN00038802.Componentes;

namespace T1CalidadN00038802
{
    public class SimuladorBolos
    {
        private readonly List<ClassJugador> ListJugador;
        private readonly List<ClassPuntaje> ListPuntaje;
        public SimuladorBolos()
        {
            ListJugador = new List<ClassJugador>();
            ListPuntaje = new List<ClassPuntaje>();
        }
        public void RegistrarJugador(ClassJugador jugador)
        {
            ListJugador.Add(jugador);
        }
        public List<ClassJugador> NumeroJugador()
        {
            return ListJugador;
        }
        public void RegistrarPuntaje(ClassPuntaje puntaje)
        {
            ListPuntaje.Add(puntaje);
        }

        public int ObtenerPuntaje()
        {
            int puntaje = 0;
            foreach (var jugador in ListJugador)
            {
                var lanzar = ListPuntaje.Where(o => o.IdJugador == jugador.Id).ToList();
                for (var jugada = 0; jugada < lanzar.Count(); jugada++)
                {
                    int tiro1 = lanzar[jugada].Lanza1;
                    int tiro2 = lanzar[jugada].Lanza2;
                    //puntaje = puntaje + tiro1 + tiro2;
                    if (tiro1 == 10 && lanzar.Count() - jugada > 1) //Puntaje Strike
                    {
                        if (lanzar[jugada + 1].Lanza1 == 10)
                        {
                            puntaje = puntaje + tiro1 + tiro2 + lanzar[jugada + 1].Lanza1; //+ lanzar[jugada + 1].lanzar2;
                        }
                        else
                        {
                            puntaje = puntaje + tiro1 + tiro2 + lanzar[jugada + 1].Lanza1 + lanzar[jugada + 1].Lanza2;
                        }
                    }
                    else if (tiro1 + tiro2 == 10 && lanzar.Count() - jugada > 1) //Puntaje Spiret
                    {
                        puntaje = puntaje + tiro1 + tiro2 + lanzar[jugada + 1].Lanza1;
                    }
                    else //Puntaje Comun
                    {
                        puntaje = puntaje + tiro1 + tiro2;
                    }
                }
            }
            return puntaje;
        }
    }
}
