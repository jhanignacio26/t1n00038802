﻿using System;
using System.Collections.Generic;
using System.Text;

namespace T1CalidadN00038802.Componentes
{
    public class ClassJugador
    {
        public int Id { get; set; }
        public String Nombre { get; set; }
        public List<ClassPuntaje> Punto { get; set; }
        public int Puntaje { get; set; }
    }
}
