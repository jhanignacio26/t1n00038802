﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using T1CalidadN00038802.Componentes;

namespace T1CalidadN00038802.Tests
{
    [TestFixture]
    public class SimuladorBolosTest
    {
        //PRUEBAS DE JUGADORES
        [Test]
        public void Caso1_Un_Jugador()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });

            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(1, resultado);
        }

        [Test]
        public void Caso2_Varios_Jugadores()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Carlos" });
            bolos.RegistrarJugador(new ClassJugador { Id = 2, Nombre = "Luis" });
            bolos.RegistrarJugador(new ClassJugador { Id = 3, Nombre = "Ignacio" });
            bolos.RegistrarJugador(new ClassJugador { Id = 3, Nombre = "Martinez" });

            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(4, resultado);
        }

        [Test]
        public void Caso3_Ningun_Jugador()
        {
            var bolos = new SimuladorBolos();

            var resultado = bolos.NumeroJugador().Count;
            Assert.AreEqual(0, resultado);
        }

        //PRUEBAS DE LANZAMIENTO CON 1 JUGADOR
        [Test]
        public void Caso1_2_y_5_Puntos()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan"});
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 0, Lanza2= 5 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(5, resultado);
        }

        [Test]
        public void Caso2_5_y_4_puntos()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 5, Lanza2 = 4 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(9, resultado);
        }

        [Test]
        public void Caso3_0_y_0_puntos()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 0, Lanza2 = 0 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(0, resultado);
        }

        [Test]
        public void Caso4_5_y_3_puntos()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 5, Lanza2 =  3});

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(8, resultado);
        }

        [Test]
        public void Caso5_1_y_9_puntos()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 1, Lanza2 = 9 });
            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(10, resultado);
        }

        [Test]
        public void Caso6_10_puntos()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(10, resultado);
        }

        //RPUEBAS CON VARIOS TIROS DE UN JUGADOR CON SPARE Y STRIKE
        [Test]
        public void Caso1_Puntaje_NoSpare_NoStrike()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 3, Lanza2 = 3 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 1, Lanza2 = 1 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 3 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 0, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 4 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(30, resultado);
        }

        [Test]
        public void Caso2_Puntaje_SiSpare_NoStrike()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 3, Lanza2 = 7 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 0, Lanza2 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 6 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(53, resultado);
        }

        [Test]
        public void Caso3_Puntaje_SiSpare_NoStrike()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 8, Lanza2 = 2 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 0, Lanza2 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 5, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 6 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(52, resultado);
        }

        [Test]
        public void Caso4_Puntaje_NoSpare_SiStrike()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 5, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 1 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 6 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(59, resultado);
        }

        [Test]
        public void Caso5_Puntaje_SiSpare_NoStrike_Final10_1()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 5, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 0, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 0, Lanza2 = 0 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(43, resultado);
        }

        [Test]
        public void Caso6_Puntaje_NoSpare_SiStrike_Final10_2()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 3, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 2 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 4 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(71, resultado);
        }

        [Test]
        public void Caso7_Puntaje_NoSpare_SiStrike_Final10_3()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 8, Lanza2 = 1 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 2 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 8 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(83, resultado);
        }

        [Test]
        public void Caso8_Puntaje_NoSpare_SiStrike_Final10_4()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 1, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 2 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 3, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 7, Lanza2 = 1 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(52, resultado);
        }

        [Test]
        public void Caso9_Puntaje_SiSpare_SiStrike_Final_1()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10});
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 8 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 6 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(83, resultado);
        }

        [Test]
        public void Caso10_Puntaje_SiSpare_SiStrike_Final_2()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10});
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 2, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 5 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 3 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(68, resultado);
        }


        [Test]
        public void Caso11_Puntaje_NoSpare_SiStrike_Final10_2Jugadores()
        {
            var bolos = new SimuladorBolos();
            bolos.RegistrarJugador(new ClassJugador { Id = 1, Nombre = "Jhan" });
            bolos.RegistrarJugador(new ClassJugador { Id = 2, Nombre = "Carlos" });

            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 2, Lanza1 = 10 });

            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 4, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 2, Lanza1 = 4, Lanza2 = 5 });

            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 3, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 2, Lanza1 = 8, Lanza2 = 1 });

            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 2 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 2, Lanza1 = 6, Lanza2 = 2 });

            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 10 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 2, Lanza1 = 10 });

            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 1, Lanza1 = 6, Lanza2 = 4 });
            bolos.RegistrarPuntaje(new ClassPuntaje { IdJugador = 2, Lanza1 = 6, Lanza2 = 8 });

            var resultado = bolos.ObtenerPuntaje();
            Assert.AreEqual(71, resultado);

            var resultado2 = bolos.ObtenerPuntaje();
            Assert.AreEqual(83, resultado);
        }
    }
}
